const fs = require('fs');
const gulp = require('gulp');
const through = require('through2');

function buildIndex(callback) {
  const pokedexIndexer = through.obj(function(file, enc, cb) {
    if (file.isStream()) {
      this.emit('error', new PluginError(PLUGIN_NAME, 'Streams are not supported!'));
      return cb();
    }

    if (file.isBuffer()) {
      const pokemons = JSON.parse(file.contents.toString('utf-8'));
      data = pokemons.slice(0, 151).map((pokemon) => { return {id: parseInt(pokemon.id), name: pokemon.ename}});

      file.contents = new Buffer(JSON.stringify(data), 'utf-8');
    }

    this.push(file);
    cb();
  });

  gulp.src('conf/pokedex.json')
    .pipe(pokedexIndexer)
    .pipe(gulp.dest('src/app/constants'));

  callback();
}

gulp.task("pokemon:index", buildIndex);