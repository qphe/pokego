const KEY = Symbol.for("pokemon.fake.positions");

var globalSymbols = Object.getOwnPropertySymbols(global);
var hasObj = (globalSymbols.indexOf(KEY) > -1);

const WALKING_SPEED = 0.0001;
const WALKING_TEMPO = 500;

function* walk(path) {
  for (let [index, start] of path.slice(0, -1).entries()) {
    const end = path[index + 1];
    let segmentTraveled = 0;

    while (true) {
      const dLat = end.lat() - start.lat();
      const dLng = end.lng() - start.lng();

      const dist = Math.sqrt(dLat * dLat + dLng * dLng);
      segmentTraveled += WALKING_SPEED / dist;
      console.log(segmentTraveled);

      if (segmentTraveled >= 1.0) {
        yield {latitude: end.lat(), longitude: end.lng()};
        break;
      } else {
        const interLat = start.lat() + segmentTraveled * dLat;
        const interLng = start.lng() + segmentTraveled * dLng;
        console.log([interLat, interLng]);
        yield { latitude: interLat, longitude: interLng };
      }
    }
  }
}

class FakePositions {
  walk(path) {
    if (this.isWalking)
      clearInterval(this.interval);

    this.isWalking = true;
    this.walker = walk(path);
    this.interval = setInterval(() => {
      const res = this.walker.next();
      if (res.done) {
        clearInterval(this.interval);
        this.isWalking = false;
        return;
      }

      this.positionCallback(res.value);
    }, WALKING_TEMPO);
  }

  bindPositionCallback(callback) {
    this.positionCallback = callback;
  }
}


if (!hasObj) {
  global[KEY] = new FakePositions();
}

export default function instance() {
  return global[KEY];
}