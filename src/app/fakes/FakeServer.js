// Since the Pokemon Go API doesn't work anymore,
// we make a singleton to simulate the server
import gaussian from 'gaussian';
import _ from 'lodash';
import moment from 'moment';

const KEY = Symbol.for("pokemon.fake.server");

var globalSymbols = Object.getOwnPropertySymbols(global);
var hasObj = (globalSymbols.indexOf(KEY) > -1);

class PokemonDistribution {
  constructor(center, variance, pokemonId) {
    this.distributions = center.map((c, i) => gaussian(c, variance[i]));
    this.pokemonId = pokemonId;
  }

  sample() {
    const sample = this.distributions.map(distribution => distribution.ppf(Math.random()));
    return sample;
  }
}

class PokemonServer {
  constructor() {
    this.pokemons = [];

    let center = [37.4275, -122.165];
    let stdev = 0.000005

    this.pokemonDistributions = [
      new PokemonDistribution(center, [stdev, stdev], 0),
      new PokemonDistribution(center, [stdev, stdev], 1),
      new PokemonDistribution(center, [stdev, stdev], 2),
    ];
    this.idGen = 0;

    this.previousNearbyQueryTimestamp = moment().subtract(1, "hours");
    this.nearbyQueryTimestamp = moment();

    _.times(30, this.generatePokemon.bind(this));
  }

  generatePokemon() {
    const distribution = _.sample(this.pokemonDistributions);
    const sample = distribution.sample();
    
    const timestamp = _.random(
      this.previousNearbyQueryTimestamp.valueOf(),
      this.nearbyQueryTimestamp.valueOf()
    );
    const finalTimestamp = moment(timestamp).add(8, 'minutes');

    this.pokemons.push({
      id: this.idGen,
      pokemonId: distribution.pokemonId,
      lat: sample[0],
      lng: sample[1],
      expirationTimestamp: finalTimestamp
    });

    this.idGen++;
  }

  getNearbyPokemon(lat, lng, radius, callback) {
    this.previousNearbyQueryTimestamp = this.nearbyQueryTimestamp;
    this.nearbyQueryTimestamp = moment();

    const pokemonCountBefore = this.pokemons.length;

    this.pokemons = this.pokemons.filter(pokemon => 
      moment(pokemon.expirationTimestamp).isAfter()
    );

    const addPokemonCount = pokemonCountBefore - this.pokemons.length;
    _.times(addPokemonCount, this.generatePokemon.bind(this));

    callback(this.pokemons.filter(pokemon => {
      const dX = lat - pokemon.lat;
      const dY = lng - pokemon.lng;

      return Math.sqrt(dX * dX + dY * dY) < radius;
    }));
  }
}

if (!hasObj) {
  global[KEY] = new PokemonServer();
}

export default function instance() {
  return global[KEY];
}