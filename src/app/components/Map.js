import GoogleMap from 'google-map-react';
import haversine from 'haversine';
import _ from 'lodash';
import moment from 'moment';
import React, {PropTypes, Component} from 'react';
import {connect} from 'react-redux';

import {MAPS_API_KEY} from '../constants/MapsApi';
import * as actions from '../actions/index';
import PokemonMapMarker from './PokemonMapMarker';
import FakePosition from '../fakes/FakePositions';

class Map extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		const procPosition = position => this.updatePosition(position.coord.latitude, positoin.coord.longitude);

		navigator.geolocation.getCurrentPosition(procPosition, () => {
			FakePosition().bindPositionCallback(position => {
				this.updatePosition(position.latitude, position.longitude);
			});
			if ("polyline" in this.state)
				FakePosition().walk(this.state.polyline);

		}, {timeout: 20});
		navigator.geolocation.watchPosition(procPosition);
		this.props.dispatch(actions.setJourneyStart({
			lat: this.props.lat, 
			lng: this.props.lng
		}, moment().add(3600, 's')));
	}

	updatePokemonPath() {
		this.props.dispatch(actions.fetchNearbyPokemon(
			this.props.lat, 
			this.props.lng, 1, (dispatch, closestPokemon) => {
			if ("closestPokemon" in this.state && closestPokemon.id === this.state.closestPokemon.id)
				return;
		
		  this.setState({closestPokemon: closestPokemon});
			dispatch(actions.fetchShortestPath(closestPokemon, (api, pokemon, path) => {
				if (this.state.polyline) {
					this.state.polyline.setMap(null);
				}

				const decoded = api.geometry.encoding.decodePath(path);
				const fp = FakePosition();
				if ("positionCallback" in fp)
					fp.walk(decoded);

				const polyline = new api.Polyline({
					path: decoded,
					strokeColor: '#000000',
          strokeOpacity: 1,
          strokeWeight: 3,
					map: this.state.mapObj
				});

				this.setState({polyline: polyline});
			}));
		}));
	}

	updatePosition(lat, lng) {
		this.props.dispatch(actions.setCurrentPosition({ lat: lat, lng: lng }));

		if ("closestPokemon" in this.state) {
			const dist = haversine({
				latitude: this.state.closestPokemon.lat,
				longitude: this.state.closestPokemon.lng
			}, {
					latitude: lat,
					longitude: lng
				});

			// if we are at a destination do stuff
			if (dist < 0.0001 && this.props.end.id !== undefined) {
				this.props.dispatch(actions.visitedPokemon(this.props.end));
			}
		}
		// we always update the path in case the user goes in the wrong direction
		this.updatePokemonPath();
	}

	componentDidMount() {
		this.props.dispatch(actions.fetchNearbyPokemon(this.props.lat, this.props.lng, 1));
	}

	render() {
		return (
			<GoogleMap
				className="map"
				bootstrapURLKeys={{
					key: MAPS_API_KEY,
				}}
				onGoogleApiLoaded={({map, maps}) => {
					this.setState({mapObj: map}, () => {
						this.props.dispatch(actions.setGoogleApi(maps));
            this.updatePosition(this.props.lat, this.props.lng);
					});
				}}
				yesIWantToUseGoogleMapApiInternals={true}
				center={{lat: this.props.lat, lng: this.props.lng}}
				zoom={18}>
				{_.values(this.props.pokemons).map(pokemon => {
					return <PokemonMapMarker className="marker"
						key = {pokemon.id}
						id={pokemon.id}
						lat={pokemon.lat}
						lng={pokemon.lng}
						/>;
				})}
				<PokemonMapMarker
					style={{width: 20, height: 20, color: "green"}}
					lat={this.props.lat}
					lng={this.props.lng}/>
			</GoogleMap>
		);
	}
}

Map.propTypes = {
	pokemons: PropTypes.object.isRequired,
	lat: PropTypes.number.isRequired,
	lng: PropTypes.number.isRequired
}

function mapStateToProps(state) {
	return {
		pokemons: state.pokemons.data, 
		lat: state.positions.cur.lat, 
		lng: state.positions.cur.lng
	};
}

export default connect(mapStateToProps)(Map);