import React, {PropTypes, Component} from 'react';
import {connect} from 'react-redux';

class PokemonMapMarker extends Component {
  render() {
    return (
      <div className="marker">
      </div>
    );
  }
}

PokemonMapMarker.propTypes = {
  lat: PropTypes.number,
  lng: PropTypes.number,
  id: PropTypes.number
}

function mapStateToProps(state, ownProps) {
  let pokemon = state.pokemons.data[ownProps.pokemonId];
  return Object.assign({}, ownProps);
}

export default connect(mapStateToProps)(PokemonMapMarker);