import {scaleLinear} from 'd3-scale';
import React, {Component} from 'react';

export default class PokemonHistogram extends Component {
  constructor(props) {
    super(props);

    this.state = {
      Bulbasaur: 10,
      Charmander: 5,
      Squirtle: 1,
    }
  }

  render() {
    let sortedPokemons = Object.keys(this.state).sort(
      (a, b) => this.state[a] < this.state[b]);
    let sortedPairs = sortedPokemons.map(
      name => [name, this.state[name]]);
    const bottomY = this.props.height - 
      this.props.padding - 
      this.props.imageSize - 
      this.props.imagePadding;

    const scaleXFunc = scaleLinear()
          .domain([0, sortedPairs.length])
          .range([this.props.padding, 
            this.props.width - 2 * this.props.padding]);

    const domainY = scaleLinear()
          .domain([0, Math.max(...sortedPairs.map(pair => pair[1]))]);

    const scaleYFunc = domainY.range([bottomY, this.props.padding]);
    const scaleHeight = domainY.range([0, bottomY - this.props.padding]);

    return (
      <svg
        width={this.props.width}
        height={this.props.height}>
        {sortedPairs.map((pair, index) => 
          <rect 
            key={index}
            x={scaleXFunc(index)} 
            y={bottomY - scaleYFunc(pair[1])}
            width={scaleXFunc(1)}
            height={scaleHeight(pair[1])}
            fill="green"/>)}
      </svg>
    )
  }
}