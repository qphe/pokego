import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Map from '../components/Map';
import PokemonSearch from '../components/PokemonSearch';
import PokemonHistogram from '../components/PokemonHistogram';

export default class App extends Component {
  render() {
    return (
      <div className="main">
        <PokemonSearch/>
        <Map/>
        {/*<PokemonHistogram
          width={200}
          height={200}
          padding={10}
          imageSize={50} 
          imagePadding={10}/>*/}
      </div>
    );
  }
}

App.propTypes = {
};

