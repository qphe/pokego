import Fuse from 'fuse.js';
import React, {Component} from 'react';
import Autosuggest from 'react-autosuggest';
import TagsInput from 'react-tagsinput';

import 'react-tagsinput/react-tagsinput.css'

import pokedex from '../constants/pokedex.json';

export default class PokemonSearch extends Component {
  constructor(props) {
    super(props);

    const options = {
      shouldSort: true,
      threshold: 0.3,
      location: 0,
      distance: 100,
      maxPatternLength: 32,
      minMatchCharLength: 1,
      keys: [
        "name"
      ]
    };

    this.index = new Fuse(pokedex, options);
    this.state = {
      tag: '',
      tags: [],
      suggestions: []
    };
  }

  getAutoSuggest(props) {
    const {addTag, ...other} = props;

    const handleOnChange = (e, {newValue, method}) => {
      if (method === 'enter') {
        e.preventDefault();
      } else {
        props.onChange(e);
      }
    }

    return (
      <Autosuggest
        ref={props.ref}
        suggestions={this.state.suggestions}
        onSuggestionsFetchRequested={({value}) => this.setState({suggestions: 
          this.index.search(value).slice(0, 10)})}
        onSuggestionsClearRequested={() => this.setState({suggestions: []})}
        getSuggestionValue={(suggestion) => suggestion.name}
        renderSuggestion={(suggestion) => <span>{suggestion.name}</span>}
        inputProps={{...other, onChange: handleOnChange}}
        onSuggestionSelected={(e, {suggestion}) => {
          props.addTag(suggestion.name)
        }}
      />
    )
  }

  render() {
    return (
      <div className="tag-container">
        <TagsInput
          renderInput={this.getAutoSuggest.bind(this)}
          value={this.state.tags}
          onChange={(tags) => this.setState({ tags: tags })}
          inputValue={this.state.tag}
          onChangeInput={(input) => {
            if (input.trim().length !== input.length) {
              this.setState({ tag: '', tags: [...this.state.tags, input] });
            } else {
              this.setState({ tag: input });
            }
          }} />
      </div>
    );
  }
}