import haversine from 'haversine';
import moment from 'moment';

import * as types from '../constants/ActionTypes';
import {MAPS_URL, MAPS_API_KEY} from '../constants/MapsApi';
import FakeServer from '../fakes/FakeServer';

export function setGoogleApi(api) {
  return {type: types.SET_GOOGLE_API, api: api};
}

export function requestNearbyPokemon() {
  return {type: types.REQUEST_NEARBY_POKEMON};
}

export function recieveNearbyPokemon(pokemons) {
  return {type: types.RECIEVE_NEARBY_POKEMON, pokemons: pokemons};
}

export function visitedPokemon(pokemon) {
  return {type: types.VISIT_POKEMON, pokemon: pokemon};
}

export function setJourneyStart(position, time) {
  return {type: types.SET_START_JOURNEY, start: position, travelTime: time};
}
export function setCurrentPosition(position) {
  return {type: types.SET_CUR_POSITION, position: position};
}

export function fetchShortestPath(destination, callback) {
  return (dispatch, getState) => {
    const state = getState();
    const api = state.google.api;

    state.google.directionService.route({
      origin: state.positions.cur,
      destination: new api.LatLng(destination.lat, destination.lng),
      travelMode: 'WALKING'
    }, direction => {
      callback(api, destination, direction.routes[0].overview_polyline);
    });
  }
}

export function fetchNearbyPokemon(lat, lng, radius, callback) {
  return (dispatch, getState) => {
    dispatch({type: types.REQUEST_NEARBY_POKEMON});
    FakeServer().getNearbyPokemon(lat, lng, radius, function(pokemons) {
      dispatch(recieveNearbyPokemon(pokemons));

      const curPoint = getState().positions.cur;
      const closestPokemons = Object.values(getState().pokemons.data).slice(0);

      closestPokemons.sort((a, b) => {
        function getPoint(obj) { return { latitude: obj.lat, longitude: obj.lng }; }
        return haversine(getPoint(a), getPoint(curPoint)) < haversine(getPoint(b), getPoint(curPoint));
      });

      if (callback !== undefined)
        callback(dispatch, closestPokemons[0]);
    }); 
  };
}