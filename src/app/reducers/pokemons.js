import _ from 'lodash';
import {REQUEST_NEARBY_POKEMON, RECIEVE_NEARBY_POKEMON, VISIT_POKEMON} from '../constants/ActionTypes';

export default function pokemons(state = {data: {}}, action) {
  switch (action.type) {
    case REQUEST_NEARBY_POKEMON:
      return {...state, loading: true};
    case RECIEVE_NEARBY_POKEMON:
      return {...state, 
        loading: false,
        data: Object.assign(
          state.data, 
          _.zipObject(action.pokemons.map(val => val.id), action.pokemons))
      };
    case VISIT_POKEMON:
      const pokemonId = action.pokemon.id;
      return {...state, 
        data: {...state.data, 
          pokemonId: action.pokemon
        }
      };
    default:
      return state;
  }
}