import {SET_START_JOURNEY, SET_CUR_POSITION, SET_DESTINATION} from '../constants/ActionTypes';

export default function positions(state = {
  travelTime: 0,
  origin: {lat: 0, lng: 0},
  cur: {lat:37.4275, lng: -122.165}, 
  end: {lat: 0, lng: 0}
}, action) {
  switch (action.type) {
    case SET_START_JOURNEY:
      return {...state, 
        travelTime: action.travelTime,
        origin: action.start,
        cur: action.start 
      };
    case SET_CUR_POSITION:
      return {...state, cur: action.position};
    default:
      return state;
  }
}