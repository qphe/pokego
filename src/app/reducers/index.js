import {combineReducers} from 'redux';
import pokemons from './pokemons';
import positions from './positions';
import google from './google';

const rootReducer = combineReducers({
  pokemons,
  positions,
  google
});

export default rootReducer;
