import {SET_GOOGLE_API} from '../constants/ActionTypes';

export default function google(state = {}, action) {
  switch(action.type) {
    case SET_GOOGLE_API:
      return {...state, 
        api: action.api,
        directionService: new action.api.DirectionsService(),
        distanceMatrixService: new action.api.DistanceMatrixService()
      };
    default:
      return state;
  }
}