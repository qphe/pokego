import 'babel-polyfill';

import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import App from './app/components/App';
import configureStore from './app/store/configureStore';
import {Router, Route, browserHistory} from 'react-router';

import './index.scss';

const store = configureStore();

render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path="/" component={App}/>
      {/*<Route path="/test" 
      component={<div style="width:100px;height:100px; background-color:red;"></div>}/>*/}
    </Router>
  </Provider>,
  document.getElementById('root')
);
